#!/bin/python

import csv
import numpy as np
import matplotlib.pyplot as plt

outfile = 'age-domain.svg'

def read_csv(csvfile):
    with open(csvfile, 'r') as file_:
        reader = csv.reader(file_, delimiter=',')
        vals = np.array([(int(row[0]),
                          int(row[1]),
                          int(row[2]))
                         for row in reader])
        return vals[:,0], vals[:,1], vals[vals[:,2] > 0, 0]

def stepify(xs, ys):
    return np.repeat(xs, 2)[1:], np.repeat(ys, 2)[:-1]

def main():
    from dateutil.rrule import MO
    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(7, 3)
    ymax = 4

    ax.set_xlabel("Age (days)")
    ax.set_ylabel("Number of disks")
    ax.set_xlim(0, 1096);
    ax.set_xticks([0,365,500,730,1095])
    ax.set_ylim(0, ymax);
    ax.set_yticks([0,1,2,3,4])

    xs, ys, fxs = read_csv('logs-agetime.csv')
    ax.vlines(fxs, 0, ymax, colors='#ee7777', linestyles='dashed')
    xs, ys = stepify(xs, ys)
    ax.fill_between(xs, ys, color='#0766aa33')
    fig.savefig(outfile, bbox_inches='tight')

if __name__ == '__main__':
    main()

